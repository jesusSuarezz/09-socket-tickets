const express = require('express');

//Libreria de los  socket
const socketIO = require('socket.io');

const http = require('http');

const path = require('path');

//Inicializamos express
const app = express();

//Creamos el servidor con las config de express
let server = http.createServer(app);

//Hacemos publica la carpeta public
const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

//Middleware para habilitar la carpeta public
app.use(express.static(publicPath));

//Inicializamos el socketIO
// IO = Esta es la conexion con el backend
module.exports.io = socketIO(server);

require('./sockets/socket');

//Para saber si el cliente(frontend) ya se conecto a nuestro servidor
//el parametro "client" contiene toda la inf del cliente que se conecto

//Es el puerto 3000 en el que se correra nuesta app
server.listen(port, (err) => {
	if (err) throw new Error(err);

	console.log(`Servidor corriendo en puerto ${port}`);
});
