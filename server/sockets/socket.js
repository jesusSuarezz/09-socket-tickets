//Importamos el modulo IO desde el server para tener acceso a el
const { io } = require('../server');

const { TicketControl } = require('../classes/ticket-control');

//Disparamos el contructor de TicketControl
const ticketControl = new TicketControl();

//Escucha la informacion de los clientes que se conecten
io.on('connection', (client) => {
	//console.log('Usuario conectado');

	client.on('siguienteTicket', (data, callback) => {
		let siguiente = ticketControl.siguienteTicket();
		console.log(siguiente);
		callback(siguiente);
	});

	//Emitir un evento 'estadoActual
	//Los emits son para enviar informacion
	client.emit('estadoActual', {
		actual: ticketControl.getUltimoTicket(),
		getUltimos4: ticketControl.getUltimos4(),
	});

	client.on('atenderTicket', (data, callback) => {
		//Validamos que en la funcion venga el escritorio que atendera el ticket
		if (!data.escritorio) {
			return callback({
				error: true,
				mensaje: 'El escritorio es necesario',
			});
		}

		let atenderTicket = ticketControl.atenderTicket(data.escritorio);

		callback(atenderTicket);

		//Actualizar/notificar cambios en los ULTIMOS 4 a todos los escritorios
		client.broadcast.emit('ultimos4', {
			getUltimos4: ticketControl.getUltimos4(),
		});
	});
});
