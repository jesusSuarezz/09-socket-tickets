const fs = require('fs');

//para crear el numero de ticket y cual es el escritorio(usuario) que lo atendio
class Ticket {
	constructor(numero, escritorio) {
		this.numero = numero;
		this.escritorio = escritorio;
	}
}

class TicketControl {
	//El contructor se ejecuta cuendo declaramos la dig. linea. let ticketControl = new TicketControl
	//el contructor es el lugar sonde se definen las propiedades y lo que se va hacer cuando se inicialice esta clase
	constructor() {
		this.ultimo = 0;
		this.hoy = new Date().getDate();

		//Contiene los tickets pendientes por atentder
		this.tickets = [];
		this.ultimosCuatroTickets = []; //para atender

		//Leemos los datos del data.json
		let data = require('../data/data.json');

		if (data.hoy === this.hoy) {
			//Si estamos en el mismo dia se mantiene la facha actual
			this.ultimo = data.ultimo;
			this.tickets = data.tickets;
			this.ultimosCuatroTickets = data.ultimosCuatroTickets;
		} else {
			this.reiniciarConteo();
		}
	}

	//Regresa el ultimo numero de ticket
	siguienteTicket() {
		this.ultimo += 1;

		//Creamos una instancia de un ticket
		//Ocupamos el contructor para crear un nuevo ticket
		let ticket = new Ticket(this.ultimo, null);
		//añadimos el ticket nuevo al array de la BD (data.json)
		this.tickets.push(ticket);

		this.grabarArchivo();

		return `Ticket ${this.ultimo}`;
	}

	//Regresa el ultimo ticket generado
	getUltimoTicket() {
		return `Ticket ${this.ultimo}`;
	}

	getUltimos4() {
		return this.ultimosCuatroTickets;
	}

	//Cuando se le da click en el boton "Atender Siguiente Ticket"
	atenderTicket(escritorio) {
		//Si no hay tickets por atender
		if (this.tickets.length === 0) {
			return 'No hay tickets';
		}

		//Extraemos el numero para romper la relacion con que los objetos en JS son basados por referencia
		//Revisamos cual es el primer ticket de la cola del array
		let numeroTicket = this.tickets[0].numero;
		//Eliminamos el ticket que vamos a atender (La primera posicion del array)
		this.tickets.shift();

		//Modificamos el objeto para decirle cual es el escritorio que lo va a atender
		let atenderTicket = new Ticket(numeroTicket, escritorio);

		//Ponemos los ultimos cuatro tickets al inicio del areglo
		this.ultimosCuatroTickets.unshift(atenderTicket);

		//Verificamos que solo existan 4 tickets es el array
		if (this.ultimosCuatroTickets.length > 4) {
			//Removemos el ultimo elemento del areglo
			this.ultimosCuatroTickets.splice(-1, 1);
		}

		console.log('Ultimos cuatro');
		console.log(this.ultimosCuatroTickets);

		this.grabarArchivo();

		//Retornamos cual es el ticket que vamos a atender
		return atenderTicket;
	}

	reiniciarConteo() {
		this.ultimo = 0;
		//console.log('Se ha inicializado el sistema');
		this.tickets = [];
		this.ultimosCuatroTickets = [];

		console.log('Se ha inicializado el sistema');
		//Grabamos en la base de datos
		this.grabarArchivo();
	}

	grabarArchivo() {
		let jsonData = {
			ultimo: this.ultimo,
			hoy: this.hoy,
			tickets: this.tickets,
			ultimosCuatroTickets: this.ultimosCuatroTickets,
		};

		let jsonDataString = JSON.stringify(jsonData);

		//Sobreescribimos los datos
		fs.writeFileSync('./server/data/data.json', jsonDataString);
	}
}

module.exports = { TicketControl };
