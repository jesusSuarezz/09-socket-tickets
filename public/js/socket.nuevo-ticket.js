// Comando para establecer la conexion
var socket = io();

var label = $('#lblNuevoTicket');

//Si ya se conecto imprime...
socket.on('connect', () => {
	console.log('conectado al servidor');
});

//Los ON son para escuchar informacion(cambios)
socket.on('disconnect', () => {
	console.log('Upps servidor desconectado...');
});

//Con jquery asemos el selector del boton
//Todos los botones al hacer click en esta pantalla disparan la funcion
$('button').on('click', () => {
	console.log('click en el button');
	socket.emit('siguienteTicket', null, (siguienteTicket) => {
		//console.log(siguienteTicket);
		label.text(siguienteTicket);
	});
});

//Los ON son para escuchar informacion(cambios) que vienen de los emit
socket.on('estadoActual', (respuesta) => {
	//console.log(respuesta);
	label.text(respuesta.actual);
});
