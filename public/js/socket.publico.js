// Comando para establecer la conexion
var socket = io();

//Con jquery hacemos selectores para cada elemento html donde se mostrara inf.
let lblTicket1 = $('#lblTicket1');
let lblTicket2 = $('#lblTicket2');
let lblTicket3 = $('#lblTicket3');
let lblTicket4 = $('#lblTicket4');

let lblEscritorio1 = $('#lblEscritorio1');
let lblEscritorio2 = $('#lblEscritorio2');
let lblEscritorio3 = $('#lblEscritorio3');
let lblEscritorio4 = $('#lblEscritorio4');

const lblTickets = [lblTicket1, lblTicket2, lblTicket3, lblTicket4];
const lblEscritorios = [
	lblEscritorio1,
	lblEscritorio2,
	lblEscritorio3,
	lblEscritorio4,
];

const actualizaHTML = (getUltimos4) => {
	for (let i = 0; i <= getUltimos4.length - 1; i++) {
		lblTickets[i].text('Ticket ' + getUltimos4[i].numero);
		lblEscritorios[i].text('Escritorio ' + getUltimos4[i].escritorio);
	}
};

socket.on('estadoActual', (data) => {
	console.log(data);
	actualizaHTML(data.getUltimos4);
});

socket.on('ultimos4', (data) => {
	//REproducimos nuestro audio cuando cambia de turno
	const audio = new Audio('audio/new-ticket.mp3');
	audio.play();

	actualizaHTML(data.getUltimos4);
});
