// Comando para establecer la conexion
var socket = io();

//Buscamos los parametros que trae nuestra url que pasamos de la pantalla index.html
var searchParams = new URLSearchParams(window.location.search);

//Con la Fn .has preguntamos si hay un parametro en con nombre escritorio
if (!searchParams.has('escritorio')) {
	//Si, si existe un error enviamos al usuario a la ruta index.html con un error
	window.location = 'index.html';

	//Se usa trhow y no return porque no estamos dentro de una funcion, por lo tanto no nos podemos salir
	throw new Error('El escritorio es necesario');
}

var escritorio = searchParams.get('escritorio');
//console.log(escritorio);
var small = $('small');

//Con jquery mostramos el sig. msg
$('h1').text(`Escritorio: ${escritorio}`);

$('button').on('click', () => {
	socket.emit('atenderTicket', { escritorio: escritorio }, (respuesta) => {
		console.log(respuesta);

		if (respuesta === 'No hay tickets') {
			alert(respuesta);
			small.text(respuesta);

			return;
		}
		small.text(`Ticket: ${respuesta.numero}`);
	});
});
